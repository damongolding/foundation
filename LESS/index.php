<!DOCTYPE html>
<?php
require "lessc.inc.php";

$less = new lessc;
$less->checkedCompile("less/styles.less", "css/styles.css");

?>
<html lang="en">
	<head>
		<title></title>
		<meta charset="utf-8" />
		<link type="text/css" rel="stylesheet" href="css/styles.css" />
		
	</head>
	<body>
	
	<div class="foo">
		<div class="far"></div>
	</div>
		
	</body>
</html>
